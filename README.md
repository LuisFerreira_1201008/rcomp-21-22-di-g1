RCOMP 2021-2022 Project repository
===========================================
# 1. Team members #
  * 1201008 - {Luís Ferreira} 
  * 1200972 - {José Rocha} 
  * 1200871 - {José Monteiro}  
  * 1181122 - {Francisca Morais}  

# 2. Team membership changes 

Member 1181122 (Francisca Morais) has joined the team on 2022-03-18

# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

