RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1200871 ###
# 1. Sprint's backlog #
| Task | Description |
| :---: | :---: |
| T.2.1 | Development of a layer two and layer three Packet Tracer simulation for building one, encompassing the campus backbone. Integration of every member´s Packet Tracer simulation into a single simulation|
| T.2.2 | Development of a layer two and layer three Packet Tracer simulation for building two, encompassing the campus backbone. |
| T.2.3 | Development of a layer two and layer three Packet Tracer simulation for building three, encompassing the campus backbone. |
| T.2.4 | Development of a layer two and layer three Packet Tracer simulation for building four, encompassing the campus backbone. |
| T.2.5 | Development of a layer two and layer three Packet Tracer simulation for building five, encompassing the campus backbone. |

# 2. Subtasks assessment #

## 2.1. 1201008 - Packet Tracer simulation for building three, encompassing the campus backbone. #
### Totally implemented with no issues. ###

## 2.2. 1200871 - Packet Tracer simulation for building two, encompassing the campus backbone. #
### Totally implemented with no issues. ###

## 2.3. 1200972 - Packet Tracer simulation for building one, encompassing the campus backbone. Integration of every member´s Packet Tracer simulation into a single simulation #
### Totally implemented with no issues. ###

## 2.4. 1181122 - Packet Tracer simulation for building four, encompassing the campus backbone. #
###So far, nothing implemented. ###

