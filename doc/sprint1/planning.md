RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1201008 ###
# 1. Sprint's backlog #
| Task | Description |
| :---: | :---: |
| T.1.1 | Development of a structured cabling project for building 1,encompassing the campus backbone. |
| T.1.2 | Development of a structured cabling project for building 2. |
| T.1.3 | Development of a structured cabling project for building 3. |
| T.1.4 | Development of a structured cabling project for building 4. |
| T.1.5 | Development of a structured cabling project for building 5. |


# 2. Technical decisions and coordination #


* Single-mode fiber optic cables on the outside of the buildings and copper cables in the inside, but fiber can be used in exceptions, to increase
  the data transmission speed.


* 2 outlets every 10 m².


* Single-mode fiber between IC , HC and CP.


* Single-mode fiber in the connections between buildings.


* CAT7 type copper cable from the CP to the outlets.


* Routers (from Access Points) at a distance of +/- 50 meters in diameter, and in case of overlap, tuned to different channels (1,6 and 11).


* At any point in a room, you can find an outlet within a radius of 3 meters.


* Enclosures with +50% of the size needed to place patch panels.


* Enclosures with +50% of the size needed to place patch panels.


* For copper cables, color code T-568A will be used.


* Copper cable connectors and network outlets shall be ISO 8877 type.


# 3. Subtasks assignment #

* 1200972 - Structured cable design for building 1
* 1200871 - Structured cable design for building 2
* 1201008 - Structured cable design for building 3
* 1181122 - Structured cable design for building 4
* All team members do the connections between buildings
