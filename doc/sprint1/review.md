RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1201008 ###
# 1. Sprint's backlog #
| Task | Description |
| :---: | :---: |
| T.1.1 | Development of a structured cabling project for building 1. |
| T.1.2 | Development of a structured cabling project for building 2. |
| T.1.3 | Development of a structured cabling project for building 3. |
| T.1.4 | Development of a structured cabling project for building 4. |
| T.1.5 | Development of a structured cabling project for building 5. |
| T.1.6 | Development of a structured cabling project for campus backbone. |

# 2. Subtasks assessment #

## 2.1. 1201008 - Structured cable design for building 3, floors 0 and 1 #
### Totally implemented with no issues. ###

## 2.2. 1200972 - Structured cable design for building B, floors 0 and 1 #
### Totally implemented with no issues. ###

## 2.3. 1200871 - Structured cable design for building B, floors 0 and 1 #
### Totally implemented with no issues. ###

## 2.4. 1181122 - Structured cable design for building B, floors 0 and 1 #
###So far, nothing implemented. ###
## 2.5. All team - Structured cable design for campus backbone #
### Totally implemented with no issues. ###


