RCOMP 2021-2022 Project - Sprint 1 - All Team folder
===========================================

##Campus Plant
![CampusPlant.jpg](Campus_Plant.jpg)

###Technical decisions

* The team decided that the entire cable structure used outside the buildings from the operator's network supply point to the Main cross connect, and from there, to the Intermediate cross-connect, will be made up of single-mode fiber optic cables to ensure higher transmission rates.


* To prevent the lack of network in an entire building due to, for example, a break in the floor due to works that would damage the fiber optic cables, the team decided to divide the fiber optic cables that connect the MC to the ICs of each building into two different paths.


* To balance the transmission load (**Redundancy**), the team decided to run 3 different cables between the MC and the ICs, through the two different paths.



##Inventory


### From Operator Supply Point to Building 1 MC

#####Path 1
 
     - It is needed 15.7 m of single-mode optical fiber cable.

#####Path 2

     - It is needed 70.7 m of single-mode optical fiber cable.



### From Building 1 MC to Building 2 IC

#####Path 1

     - It is needed 42.8 m of single-mode optical fiber cable.

#####Path 2

     - It is needed 295.6 m of single-mode optical fiber cable.



### From Building 1 MC to Building 3 IC

#####Path 1

     - It is needed 99.9 m of single-mode optical fiber cable.

#####Path 2

     - It is needed 238.5 m of single-mode optical fiber cable.



### From Building 1 MC to Building 4 IC

#####Path 1

     - It is needed 166.3 m of single-mode optical fiber cable.

#####Path 2

     - It is needed 172.1 m of single-mode optical fiber cable.



### From Building 1 MC to Building 5 IC

#####Path 1

     - It is needed 223.4 m of single-mode optical fiber cable.

#####Path 2

     - It is needed 115 m of single-mode optical fiber cable.


### Total
*To prevent errors, we decided to increase 5 meters of cable in each path.*

For the cable structure on the outside of the buildings, a total of 1490 meters of single-mode fiber optic cable is required.
     