RCOMP 2021-2022 Project - Sprint 3 - Member 1200871 folder
===========================================

## Building 2 ##

### OSPF  ###

Static routing tables from last sprint were deleted,except the default gateway of the MC router of building 1.

Each building will have as the OSPF area the number corresponding to the building.

In the case of building 2, area 2 will be used. Area 0 will be used for the backbone network. Process ID = 1.

Used commands:

    router ospf 1
    network 172.17.129.0 0.0.0.127 area 0 (Backbone netwrok)
    network 172.17.132.32 0.0.0.31 area 2 (Floor 0 network)
    network 172.17.131.0 0.0.0.63 area 2 (Floor 1 network)
    network 172.17.128.128 0.0.0.127 area 2 (WiFi network)
    network 172.17.132.96 0.0.0.15 area 2 (DMZ network)
    network 172.17.132.112 0.0.0.15 area 2 (VoIP network)

### HTTP  ###

It was added to this simulation a new server and define statically its IP address (Server-PT HTTP - 172.17.132.102).

Then we configured the HTML page in order to identify the building where this HTTP server is stored in.

### DNS  ###

The member in charge of building 1 was responsible for creating a DNS domain name corresponding to the team's repository name (rcomp-21-22-di-g1).

Then each team member in charge of other buildings (Building 2 and Building 3) had to create local subdomains.

In my case :

    building-2.rcomp-21-22-di-g1

Unqualified DNS name:

    ns.building-2.rcomp-21-22-di-g1

DNS Table :

![DNS_Table](./DNS Table.png)

### DHCPv4 service ###

Only for Floor 0 , Floor 1 , WiFi and VoIP networks were provided DCHP services.

#### End nodes configuration: ####

    ip dhcp pool VLANB2F0
    network 172.17.132.32 255.255.255.224
    default-router 172.17.132.35
    dns-server 172.17.132.100
    domain-name building-2.rcomp-21-22-di-g1

    ip dhcp pool VLANB2F1
    network 172.17.131.0 255.255.255.192
    default-router 172.17.131.3
    dns-server 172.17.132.100
    domain-name building-2.rcomp-21-22-di-g1

    ip dhcp pool VLANB2WiFi
    network 172.17.128.128 255.255.255.128
    default-router 172.17.128.131
    dns-server 172.17.132.100
    domain-name building-2.rcomp-21-22-di-g1

    ip dhcp pool VLANVoIP
    network 172.17.132.112 255.255.255.240
    default-router 172.17.132.115
    option 150 ip 172.17.132.115
    dns-server 172.17.132.100
    domain-name building-2.rcomp-21-22-di-g1 


### VoIP service ###

To configure VoIP service, the highest level router of building was used.

2 IP Phones were used.

Commands:

    telephony-service
    no auto-reg-ephone
    max-ephones 2
    max-dn 2
    ip source-address 172.17.132.115 port 2000

    ephone-dn 1
    number 1000

    ephone-dn 2
    number 1001

    ephone 1
    type 7960
    button 1:1

    ephone 2
    type 7960
    button 1:2

### NAT  ###

To configure NAT those commands were used:

    For each router sub-interface that contains an IP of the building's local area networks:
    ip nat inside

    To the router sub-interface that contains the backbone network IP:
    ip nat outside

    ip nat inside source static tcp 172.17.132.102 80 172.17.132.99 80 
    ip nat inside source static tcp 172.17.132.102 433 172.17.132.99 433
    ip nat inside source static tcp 172.17.132.100 53 172.17.132.99 53
    ip nat inside source static udp 172.17.132.100 53 172.17.132.99 53 

### FIREWALL  ###


1 st - Block all spoofing, as far as possible. Internal spoofing from local networks, the DMZ may be
excluded. External spoofing in traffic incoming from the backbone network.

    Floor 0 network
    access-list 100 permit ip 172.17.132.32 0.0.0.31 any 

    Floor 1 network
    access-list 101 permit ip 172.17.131.0 0.0.0.63 any

    WiFi network
    access-list 102 permit ip 172.17.130.192 0.0.0.63 any

    VoIP network
    access-list 103 permit ip 172.17.132.112 0.0.0.15 any

    Backbone network
    access-list 105 deny ip 172.17.132.32 0.0.0.31 any
    access-list 105 deny ip 172.17.131.0 0.0.0.63 any
    access-list 105 deny ip 172.17.128.128 0.0.0.127 any
    access-list 105 deny ip 172.17.132.112 0.0.0.15 any
    access-list 105 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0

2 nd - All ICMP echo requests and echo replies are always allowed

    access-list 100 permit icmp any any echo
    access-list 100 permit icmp any any echo-reply
    access-list 101 permit icmp any any echo
    access-list 101 permit icmp any any echo-
    access-list 101 permit icmp any any echo-reply
    access-list 102 permit icmp any any echo
    access-list 102 permit icmp any any echo-reply
    access-list 103 permit icmp any any echo
    access-list 103 permit icmp any any echo-reply
    access-list 105 permit icmp any any echo
    access-list 105 permit icmp any any echo-reply

3 rd - All traffic to the DMZ is to be blocked, except for the DNS service and HTTP/HTTPS services
to the corresponding servers. All traffic incoming from the DMZ is allowed.

    access-list 106 permit tcp any 172.17.132.102 0.0.0.0 eq 80 
    access-list 106 permit tcp any 172.17.132.102 0.0.0.0 eq 443
    access-list 106 permit tcp any 172.17.132.100 0.0.0.0 eq 53
    access-list 106 permit udp any 172.17.132.100 0.0.0.0 eq 53
    access-list 106 permit ip 172.17.132.96 0.0.0.15 any

4 th - All traffic directed to the router itself (with a destination IPv4 node address belonging to the
router) is to be blocked, except for the traffic required for the current features to work.


    access-list 106 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    access-list 105 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    access-list 103 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    access-list 102 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    access-list 101 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    access-list 100 permit ip 0.0.0.0 255.255.255.255 255.255.255.255 0.0.0.0
    
    access-list 105 permit udp any 172.17.129.3 0.0.0.0 eq 69
    access-list 100 permit udp any 172.72.132.35 0.0.0.0 eq 69
    access-list 101 permit udp any 172.72.131.3 0.0.0.0 eq 69
    access-list 102 permit udp any 172.72.128.131 0.0.0.0 eq 69
    access-list 103 permit udp any 172.72.132.115 0.0.0.0 eq 69
    access-list 106 permit udp any 172.17.132.99 0.0.0.0 eq 69
    
    access-list 105 permit tcp any 172.17.129.3 0.0.0.0 eq 2000
    access-list 105 permit tcp any 172.17.129.3 0.0.0.0 eq 1720
    access-list 100 permit tcp any 172.72.132.35 0.0.0.0 eq 2000
    access-list 101 permit tcp any 172.72.131.3 0.0.0.0 eq 2000
    access-list 102 permit tcp any 172.72.128.131 0.0.0.0 eq 2000
    access-list 103 permit tcp any 172.72.132.115 0.0.0.0 eq 2000
    access-list 106 permit tcp any 172.17.132.99 0.0.0.0 eq 2000
    
    access-list 100 permit ospf any 224.0.0.5 0.0.0.0
    access-list 101 permit ospf any 224.0.0.5 0.0.0.0
    access-list 102 permit ospf any 224.0.0.5 0.0.0.0
    access-list 103 permit ospf any 224.0.0.5 0.0.0.0
    access-list 105 permit ospf any 224.0.0.5 0.0.0.0
    access-list 106 permit ospf any 224.0.0.5 0.0.0.0
    
    access-list 105 permit tcp any host 172.17.129.3 eq 80
    access-list 105 permit tcp any host 172.17.129.3 eq 443
    access-list 105 permit tcp any host 172.17.129.3 eq 53
    access-list 105 permit udp any host 172.17.129.3 eq 53
    
    access-list 105 deny ip any 172.17.129.3 0.0.0.0
    access-list 100 deny ip any 172.17.129.3 0.0.0.0
    access-list 101 deny ip any 172.17.129.3 0.0.0.0
    access-list 102 deny ip any 172.17.129.3 0.0.0.0
    access-list 103 deny ip any 172.17.129.3 0.0.0.0
    access-list 106 deny ip any 172.17.129.3 0.0.0.0

5 th - Remaining traffic passing through the router should be allowed

    access-list 100 permit ip any any
    access-list 101 permit ip any any
    access-list 102 permit ip any any
    access-list 103 permit ip any any
    access-list 105 permit ip any any
    access-list 106 permit ip any any
    
    int f1/0.16
    ip access-group 105 in
    int f1/0.11
    ip access-group 100 in
    int f1/0.12
    ip access-group 101 in
    int f1/0.13
    ip access-group 102 in
    int f1/0.15
    ip access-group 103 in
    int f1/0.14
    ip access-group 106 in