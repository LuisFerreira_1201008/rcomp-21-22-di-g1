!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017R6R1-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 350
 ip address 172.17.131.193 255.255.255.192
!
interface FastEthernet1/0.2
 encapsulation dot1Q 351
 ip address 172.17.131.65 255.255.255.192
!
interface FastEthernet1/0.3
 encapsulation dot1Q 352
 ip address 172.17.130.193 255.255.255.192
!
interface FastEthernet1/0.4
 encapsulation dot1Q 353
 ip address 172.17.132.1 255.255.255.224
!
interface FastEthernet1/0.5
 encapsulation dot1Q 354
 ip address 172.17.132.65 255.255.255.224
!
interface FastEthernet1/0.6
 encapsulation dot1Q 340
 ip address 172.17.130.129 255.255.255.192
!
interface FastEthernet1/0.7
 encapsulation dot1Q 341
 ip address 172.17.130.1 255.255.255.128
!
interface FastEthernet1/0.8
 encapsulation dot1Q 342
 ip address 172.17.128.1 255.255.255.128
!
interface FastEthernet1/0.9
 encapsulation dot1Q 343
 ip address 172.17.129.129 255.255.255.128
!
interface FastEthernet1/0.10
 encapsulation dot1Q 344
 ip address 172.17.131.129 255.255.255.192
!
interface FastEthernet1/0.11
 encapsulation dot1Q 345
 ip address 172.17.132.33 255.255.255.224
!
interface FastEthernet1/0.12
 encapsulation dot1Q 346
 ip address 172.17.131.1 255.255.255.192
!
interface FastEthernet1/0.13
 encapsulation dot1Q 347
 ip address 172.17.128.129 255.255.255.128
!
interface FastEthernet1/0.14
 encapsulation dot1Q 348
 ip address 172.17.132.97 255.255.255.240
!
interface FastEthernet1/0.15
 encapsulation dot1Q 349
 ip address 172.17.132.113 255.255.255.240
!
interface FastEthernet1/0.16
 encapsulation dot1Q 355
 ip address 172.17.129.1 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

