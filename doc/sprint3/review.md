RCOMP 2021-2022 Project - Sprint 3 review
=========================================
### Sprint master: 1200972 ###
# 1. Sprint's backlog #
| Task | Description |
| :---: | :---: |
| T.3.1 | Update the building1.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 1.|
| T.3.2 | Update the building2.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 2. Final integration of each member’s Packet Tracer simulation into a single simulation (campus.pkt). |
| T.3.3 |  Update the building3.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 3 |
| T.3.4 | Update the building4.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 4 |
| T.3.5 |  Update the building5.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 5 |

# 2. Subtasks assessment #

## 2.1. 1201008 - Packet Tracer simulation for building three, encompassing the campus backbone. #
### Totally implemented with no issues. ###

## 2.2. 1200871 - Packet Tracer simulation for building two, encompassing the campus backbone. Integration of every member´s Packet Tracer simulation into a single simulation  #
### Totally implemented with no issues. ###

## 2.3. 1200972 - Packet Tracer simulation for building one, encompassing the campus backbone. #
### Totally implemented with no issues. ###

## 2.4. 1181122 - Packet Tracer simulation for building four, encompassing the campus backbone. #
###So far, nothing implemented. ###


