!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.17.130.130
ip dhcp excluded-address 172.17.130.129
ip dhcp excluded-address 172.17.130.1
ip dhcp excluded-address 172.17.128.1
ip dhcp excluded-address 172.17.131.129
ip dhcp excluded-address 172.17.130.2
ip dhcp excluded-address 172.17.128.2
ip dhcp excluded-address 172.17.131.130
ip dhcp excluded-address 172.17.130.131
ip dhcp excluded-address 172.17.130.3
ip dhcp excluded-address 172.17.128.3
ip dhcp excluded-address 172.17.131.131
ip dhcp excluded-address 172.17.130.133
ip dhcp excluded-address 172.17.130.5
ip dhcp excluded-address 172.17.128.5
ip dhcp excluded-address 172.17.131.133
!
ip dhcp pool VLANB1F0
 network 172.17.130.128 255.255.255.192
 default-router 172.17.130.130
 dns-server 172.17.129.132
 domain-name rcomp-21-22-2di-g1
ip dhcp pool VLANB1F1
 network 172.17.130.0 255.255.255.128
 default-router 172.17.130.2
 dns-server 172.17.129.132
 domain-name rcomp-21-22-2di-g1
ip dhcp pool VLANB1wifi
 network 172.17.128.0 255.255.255.128
 default-router 172.17.128.2
 dns-server 172.17.129.132
 domain-name rcomp-21-22-2di-g1
ip dhcp pool VLANB1VOIP
 network 172.17.131.128 255.255.255.192
 default-router 172.17.131.130
 option 150 ip 172.17.131.130
 dns-server 172.17.129.132
 domain-name rcomp-21-22-di-g1
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017DS65-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 350
 ip address 172.17.131.194 255.255.255.192
!
interface FastEthernet1/0.2
 encapsulation dot1Q 351
 ip address 172.17.131.66 255.255.255.192
!
interface FastEthernet1/0.3
 encapsulation dot1Q 352
 ip address 172.17.130.194 255.255.255.192
!
interface FastEthernet1/0.4
 encapsulation dot1Q 353
 ip address 172.17.132.2 255.255.255.224
!
interface FastEthernet1/0.5
 encapsulation dot1Q 354
 ip address 172.17.132.66 255.255.255.224
!
interface FastEthernet1/0.6
 encapsulation dot1Q 340
 ip address 172.17.130.130 255.255.255.192
 ip access-group 100 in
 ip nat inside
!
interface FastEthernet1/0.7
 encapsulation dot1Q 341
 ip address 172.17.130.2 255.255.255.128
 ip access-group 101 in
 ip nat inside
!
interface FastEthernet1/0.8
 encapsulation dot1Q 342
 ip address 172.17.128.2 255.255.255.128
 ip access-group 102 in
 ip nat inside
!
interface FastEthernet1/0.9
 encapsulation dot1Q 343
 ip address 172.17.129.130 255.255.255.128
 ip access-group 106 in
 ip nat inside
!
interface FastEthernet1/0.10
 encapsulation dot1Q 344
 ip address 172.17.131.130 255.255.255.192
 ip access-group 103 in
 ip nat inside
!
interface FastEthernet1/0.11
 encapsulation dot1Q 345
 ip address 172.17.132.34 255.255.255.224
!
interface FastEthernet1/0.12
 encapsulation dot1Q 346
 ip address 172.17.131.2 255.255.255.192
!
interface FastEthernet1/0.13
 encapsulation dot1Q 347
 ip address 172.17.128.130 255.255.255.128
!
interface FastEthernet1/0.14
 encapsulation dot1Q 348
 ip address 172.17.132.98 255.255.255.240
!
interface FastEthernet1/0.15
 encapsulation dot1Q 349
 ip address 172.17.132.114 255.255.255.240
!
interface FastEthernet1/0.16
 encapsulation dot1Q 355
 ip address 172.17.129.2 255.255.255.128
 ip access-group 105 in
 ip nat outside
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.17.129.0 0.0.0.127 area 0
 network 172.17.130.128 0.0.0.63 area 1
 network 172.17.130.0 0.0.0.127 area 1
 network 172.17.128.0 0.0.0.127 area 1
 network 172.17.129.128 0.0.0.127 area 1
 network 172.17.131.128 0.0.0.63 area 1
!
router rip
!
ip nat inside source static tcp 172.17.129.134 80 172.17.129.130 80 
ip nat inside source static tcp 172.17.129.134 433 172.17.129.130 433 
ip nat inside source static tcp 172.17.129.132 53 172.17.129.130 53 
ip nat inside source static udp 172.17.129.132 53 172.17.129.130 53 
ip classless
!
ip flow-export version 9
!
!
access-list 100 permit ip 172.17.130.128 0.0.0.63 any
access-list 100 permit icmp any any echo
access-list 100 permit icmp any any echo-reply
access-list 100 permit ip any host 255.255.255.255
access-list 100 permit udp any host 172.17.130.130 eq tftp
access-list 100 permit tcp any host 172.17.130.130 eq 2000
access-list 100 permit ospf any host 224.0.0.5
access-list 100 deny ip any host 172.17.129.2
access-list 100 permit ip any any
access-list 101 permit ip 172.17.130.0 0.0.0.127 any
access-list 101 permit icmp any any echo
access-list 101 permit icmp any any echo-reply
access-list 101 permit ip any host 255.255.255.255
access-list 101 permit udp any host 172.17.130.2 eq tftp
access-list 101 permit tcp any host 172.17.130.2 eq 2000
access-list 101 permit ospf any host 224.0.0.5
access-list 101 deny ip any host 172.17.129.2
access-list 101 permit ip any any
access-list 102 permit ip 172.17.128.0 0.0.0.127 any
access-list 102 permit icmp any any echo
access-list 102 permit icmp any any echo-reply
access-list 102 permit ip any host 255.255.255.255
access-list 102 permit udp any host 172.17.128.2 eq tftp
access-list 102 permit tcp any host 172.17.128.2 eq 2000
access-list 102 permit ospf any host 224.0.0.5
access-list 102 deny ip any host 172.17.129.2
access-list 102 permit ip any any
access-list 103 permit ip 172.17.131.128 0.0.0.63 any
access-list 103 permit icmp any any echo
access-list 103 permit icmp any any echo-reply
access-list 103 permit ip any host 255.255.255.255
access-list 103 permit udp any host 172.17.131.130 eq tftp
access-list 103 permit tcp any host 172.17.131.130 eq 2000
access-list 103 permit ospf any host 224.0.0.5
access-list 103 deny ip any host 172.17.129.2
access-list 103 permit ip any any
access-list 105 deny ip 172.17.130.128 0.0.0.63 any
access-list 105 deny ip 172.17.130.0 0.0.0.127 any
access-list 105 deny ip 172.17.128.0 0.0.0.127 any
access-list 105 deny ip 172.17.131.128 0.0.0.63 any
access-list 105 permit ip any host 255.255.255.255
access-list 105 permit icmp any any echo
access-list 105 permit icmp any any echo-reply
access-list 105 permit udp any host 172.17.129.2 eq tftp
access-list 105 permit tcp any host 172.17.129.2 eq 2000
access-list 105 permit tcp any host 172.17.129.2 eq 1720
access-list 105 permit ospf any host 224.0.0.5
access-list 105 permit tcp any host 172.17.129.2 eq www
access-list 105 permit tcp any host 172.17.129.2 eq 443
access-list 105 permit udp any host 172.17.129.2 eq domain
access-list 105 deny ip any host 172.17.129.2
access-list 105 permit ip any any
access-list 106 permit tcp any host 172.17.129.134 eq www
access-list 106 permit tcp any host 172.17.129.134 eq 443
access-list 106 permit udp any host 172.17.129.132 eq domain
access-list 106 permit ip 172.17.129.128 0.0.0.127 any
access-list 106 permit ip any host 255.255.255.255
access-list 106 permit udp any host 172.17.129.130 eq tftp
access-list 106 permit tcp any host 172.17.129.130 eq 2000
access-list 106 permit ospf any host 224.0.0.5
access-list 106 deny ip any host 172.17.129.2
access-list 106 permit ip any any
!
!
!
!
!
!
telephony-service
 no auto-reg-ephone
 max-ephones 2
 max-dn 2
 ip source-address 172.17.131.130 port 2000
!
ephone-dn 1
 number 1000
!
ephone-dn 2
 number 1001
!
ephone 1
 device-security-mode none
 mac-address 0001.C9BE.7225
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0030.F2C7.E5A8
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

